import {
  Client,
} from 'discord.js';
import fs from 'fs';
import config from './config.json';

const client = new Client();

client.on('ready', () => {
  console.log(`Add bot to server with https://discordapp.com/oauth2/authorize?client_id=${config.clientId}&scope=bot`);

  client.on('message', (msg) => {
    if (!msg.content.startsWith(config.prefix)) return;
    if (msg.author.bot) return;

    const args = msg.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    try {
      const commandFile = require(`./commands/${command}.js`);
      commandFile.run(client, msg, args);
    } catch (err) {
      console.log(err);
    }
  });
});

client.login(config.token);
