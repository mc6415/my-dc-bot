import config from '../config.json';

exports.run = (client, msg) => {
  let string = '```\n';
  config.accounts.forEach((n) => {
    string += `${n}\n\n`;
  });
  string += '```';

  msg.channel.send(string.trim());
};
