import config from '../config.json';
import Crosshair from '../models/crosshair';

function getOutput(crosshair) {
  const cross = new Crosshair(crosshair);
  const output = `  
      \`\`\`
      Type: ${cross.type}
      Thickness: ${cross.thickness}
      Crosshair Length: ${cross.crosshairLength}
      Center Gap: ${cross.centerGap}
      Opacity: ${cross.opacity}
      Outline Opacity: ${cross.outline}
      Dot Size: ${cross.dot}
      Dot Opacity: ${cross.dotOpacity}\`\`\``;

  return output;
}

function getCrosshair(hero, msg) {
  const heroName = hero.toLowerCase();
  const aimHeroes = ['soldier', 'mccree', 'hanzo', 'widow', 'mei', 'ana', 'aim', 'zarya', 'genji', 'bastion', 'pharah', 'zenyatta', 'reinhardt', 'doomfist', 'junkrat', 'default'];
  const otherHeroes = ['tracer', 'dva', 'sombra', 'd.va', 'orisa', 'hammond', 'roadhog', 'reaper'];

  if (aimHeroes.includes(heroName)) {
    console.log(getOutput(config.crosshairAim));
    msg.channel.send(getOutput(config.crosshairAim));
    return;
  }
  if (otherHeroes.includes(heroName)) {
    msg.channel.send(getOutput(config.crosshairOther));
    return;
  }
  msg.channel.send('Hero not found, please note D.Va is dva and Soldier:76 is soldier');
}


exports.run = (client, msg, args) => {
  getCrosshair(args[0], msg);
};
