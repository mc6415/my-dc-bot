class Crosshair {
  constructor(crosshair) {
    this.type = crosshair.Type;
    this.thickness = crosshair.Thickness;
    this.crosshairLength = crosshair.CrosshairLength;
    this.centerGap = crosshair.CenterGap;
    this.opacity = crosshair.Opacity;
    this.outline = crosshair.OutlineOpacity;
    this.dot = crosshair.DotSize;
    this.dotOpacity = crosshair.DotOpacity;
  }
}

export default Crosshair;
